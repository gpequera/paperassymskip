import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pingouin as pg



data =pd.read_pickle('./CoT_subset.pickle')
dicc = np.load('./regressionsMet.npy',allow_pickle=True).item()
fitcoeff = dicc['fitcoeffmean']

verde = sns.color_palette('Set2')[0]
naranja = sns.color_palette('Set2')[1]
colores = [naranja,verde]
fig,ax = plt.subplots(1,2,figsize=(6,3),sharey=False)
err_kws = {'elinewidth':.5,'capsize':2, 'ecolor':(.5,.5,.5)}
ax = ax.ravel()
hue_order = ['R','S']
# textcoordX = [2.3,2]
textcoordY = [[7,6.5],[3,4]]

for n, variable in enumerate(['pot','COT']): 
	sns.lineplot(x="velmsec", y=variable,  data=data,hue="gait", hue_order=hue_order, palette="Set2",ax=ax[n],err_style='bars', estimator=None, err_kws =err_kws, linestyle='', marker='o', units="subject", alpha=.4, markersize=4)
	sns.lineplot(x="velmsec", y=variable,  data=data,hue="gait", hue_order=hue_order, palette="Set2",ax=ax[n],ci='sd',err_style='bars', err_kws =err_kws, linestyle='', marker='o',markeredgecolor='k' )
	for i, gait in enumerate(['S','R']):
		subset = data[data.gait==gait]
		x = subset.velmsec.unique()
		fitted = fitcoeff[fitcoeff.gait.isin([gait]) & fitcoeff.variable.isin([variable])]
		poty = fitted['slope'].values*(x)+fitted['intercept'].values
		ax[n].plot(x,poty,color=colores[i])
		fittedeq = '$y = $'+str(fitted['slope'].values[0].round(2))+'$x$ + ' + str(fitted['intercept'].values[0].round(2))
		ax[n].text(2.3,textcoordY[0][n]-i/textcoordY[1][n],fittedeq, fontsize=8, color=colores[i])
	ax[n].set_xlabel('Speed (m.sec$^{-1}$)')
	ax[n].set_xticks(data.velmsec.unique())
	ax[n].set_xticklabels(data.velmsec.unique())
ax[0].set_ylabel('Metabolic power (J/kg)')
ax[1].set_ylabel('Cost of transport \n (J/kg/m)')
ax[1].text(data.velmsec.unique().min(),7.2,'B')
ax[1].text(2.7,4.5,'*', fontsize=20, color=naranja)
ax[0].text(data.velmsec.unique().min(),14,'A')
ax[0].text(2.5,12.5,'*', fontsize=20, color=naranja)
ax[0].text(3,12.5,'*', fontsize=20, color=verde)
ax[0].legend().remove()
handles, labels = ax[1].get_legend_handles_labels()
for h in handles: 
    h.set_marker('o')
    h.set_linestyle('')
    h.set_markeredgecolor('k')
l = ax[1].legend(handles[:2], ['Running', 'Skipping'], loc='best', borderaxespad=0.,frameon=False)


sns.set(style="white")
sns.set_context("paper", font_scale=1.3)
sns.despine()
sns.set_style("ticks")
plt.tight_layout()
plt.savefig('./figures/cot.pdf',bbox_inches='tight')
plt.savefig('./figures/cot.png',bbox_inches='tight')
plt.savefig('./figures/fig2.pdf',bbox_inches='tight')
plt.show()