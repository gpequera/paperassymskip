import pandas as pd
import seaborn as sns
import  matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import os
import pingouin as pg


S = pd.read_pickle('./assymetries.pickle')
R = pd.read_pickle('./assymetriesRunning.pickle')
data = pd.concat([S,R],ignore_index=True)
data['velmsec'] = np.round(data.vel/3.6,2)
# data = data[data.gait=='S']
newfolder = './figures/'
isExist = os.path.exists(newfolder)
if not isExist:
	os.makedirs(newfolder)


naranja = sns.color_palette("Oranges_r")[1]
naranja_claro = sns.color_palette("Oranges_r")[-3]
naranja_oscuro = sns.color_palette("Oranges_r")[0]
paleta = [naranja_oscuro, naranja_claro]
verde = sns.color_palette('Set2')[0]
byn = ['white','k']
ylabel = ['Step time \n asymmetry','Step length \n asymmetry']
y1label = ['Relative Step Time', 'Relative Step Length']
err_kws = {'elinewidth':.5,'capsize':2, 'ecolor':(.5,.5,.5)}
legends = [['Skipping','Running'],['Pendular step', 'Bouncing step']]
sns.set(style="white")
sns.set_context("paper", font_scale=1.3)
sns.set_style("ticks")

hue_order = ['S','R']
# figuras = []
for i,letra in enumerate(['T','L']):
	fig,ax = plt.subplots(1,2,figsize=(7.5,3),sharey=False)
	# figuras.append(fig)

	sns.lineplot(x="velmsec", y='assym'+letra.lower(), hue='gait', hue_order = hue_order, data=data, palette= [naranja, verde], ax=ax[0],estimator=None, units='subject', linewidth=.5, linestyle='', marker="o", markersize=4,alpha=.5)
	sns.lineplot(x="velmsec", y='assym'+letra.lower(), hue='gait', data=data, hue_order = hue_order, palette= [naranja, verde], ax=ax[0],ci='sd',err_style='bars', err_kws =err_kws, markersize=7, linestyle='--', marker="o", markeredgecolor='k')

	aov = pg.kruskal(dv='assym'+letra.lower(), between='vel', data=S,detailed=True)
	print('=======================================')
	print('assym'+letra.lower())
	print(aov.round(3))
	subset = data[['subject','velmsec','gait',letra+'steptr',letra+'stepld']]
	subset = subset.melt(['subject','gait','velmsec'],var_name='variable',value_name='vals')
	subset = subset[subset.gait=='S']
	sns.lineplot(x="velmsec", y='vals',hue= 'variable', data=subset, palette= paleta, ax=ax[1],units='subject', estimator=None,
				linestyle='', linewidth=.5 , style='variable',dashes=False, markers=["o",'o'], markeredgecolor='k', markersize=4, alpha=.3)
	sns.lineplot(x="velmsec", y='vals',hue= 'variable', data=subset, palette= paleta, ax=ax[1],ci='sd',err_style='bars', 
				err_kws =err_kws, linestyle='--', style='variable',dashes=False, markers=["o",'o'], markeredgecolor='k', markersize=7)
	for ii, leg in enumerate(['steptr','stepld']):
		aov = pg.kruskal(dv=letra+leg, between='vel', data=S,detailed=True)
		print('=======================================')
		print(letra+leg)
		print(aov.round(3))
		for iii in range(2):
			ax[iii].set_xlabel('Speed (m.sec$^{-1}$)')			
			ax[iii].set_yticks([0, .25, .5, .75, 1])
			ax[iii].set_yticklabels([0, .25, .5, .75, 1])
	ax[0].set_ylabel(ylabel[i])
	ax[1].set_ylabel(y1label[i])
	

	for l in [0,1]:
		ax[l].legend()
		handles, labels = ax[l].get_legend_handles_labels()
		
		for h in handles: 
		    h.set_marker('o')
		    h.set_linestyle('')
		    h.set_markeredgecolor('k')
		ax[l].legend(handles[0:2], legends[l],bbox_to_anchor=(1.1, 1.05), borderaxespad=0.,frameon=False) 

	
	if i == 1:
		ax[0].text(2.55,-0.05,'*', fontsize=20, color=naranja)
		ax[1].text(2.55,0.5,'*', fontsize=20, color=naranja_claro)
		ax[1].text(2.55,0.4,'*', fontsize=20, color=naranja_oscuro)
	ax[0].text(data.velmsec.unique().min(),1,'A')
	ax[1].text(data.velmsec.unique().min(),1,'B')
	ax[0].set_xticks(data.velmsec.unique())
	ax[0].set_xticklabels(data.velmsec.unique())
	ax[1].set_xticks(subset.velmsec.unique())
	ax[1].set_xticklabels(subset.velmsec.unique())
	plt.tight_layout()
	sns.despine()
	plt.savefig('./figures/asymmetries'+letra+'.pdf',bbox_inches='tight')
	plt.savefig('./figures/asymmetries'+letra+'.png',bbox_inches='tight')
	nfig = str(i+3)
	plt.savefig('./figures/Fig'+nfig+'.pdf',bbox_inches='tight')
plt.show()

