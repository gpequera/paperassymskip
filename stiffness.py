import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import glob
import itertools
import os


''' D.R. Coleman et al. / Journal of Biomechanics (2012), model2'''
''' Cálculo de stiffness '''


#spatiotemporal
def spatiotemporal_param(cycle,fsample=100):

	morethanthreshold = cycle.iloc[10:]>cycle.min()+0.020
	toe_off = morethanthreshold[morethanthreshold==True].index[0]
	# ss = cycle.loc[toe_off]
	cycle_samples = cycle.index[-1]-cycle.index[0]
	stride_time = cycle_samples*(1/float(fsample))
	stride_freq = 1/stride_time
	contact_time = (toe_off - cycle.index[0])*(1/fsample)
	flight_time = (cycle.index[-1] - toe_off)*(1/fsample)

# duty_factor = contact_time/np.float((cycle_dur))
	return contact_time,flight_time,stride_time

dfs = []
folder = './data/*.npy'
file_names = glob.glob(folder)
outlayers = ['FB','RI','AA']
lista = []
assymetries = pd.read_pickle('./assymetries.pickle')
for index, row in assymetries.iterrows():
	lista.append([s for s in file_names if row.file_id in s])
lista = list(itertools.chain.from_iterable(lista))
file_names_subset = lista+glob.glob('./data/*_R_*.npy')

g = -9.81 # m/s*s
prueba = glob.glob('./data/SY*.npy')
for file in file_names:
	data = np.load(file,allow_pickle=True,encoding='latin1').item()
	v = data['info']['vel (km/h)'][0]
	m = data['info']['mass (kg)'][0]
	gait = data['info']['gait'][0]
	if data['info']['trailing_leg'][0] == 'r':
		zipped = zip([data['kinematic_r'],data['kinematic_l']],['RMTz','LMTz'],['tr','ld'],['RGTz','LGTz'])
	else:
		zipped = zip([data['kinematic_l'],data['kinematic_r']],['LMTz','RMTz'],['tr','ld'],['LGTz','RGTz'])
	for ll,side,LEG,cadera in zipped:
		stiffness = []
		stiffnessCM = []
		F = []
		DL = []
		DY = []
		for nn in ll:	
			matrix = nn/1000
			n = matrix[side]
			ct, ft, st = spatiotemporal_param(n)
			leg = LEG
			L = (matrix[cadera]-n).max()
			Fmax = m*g*(np.pi/2)*((ft/ct)+1)
			deltayc = ((Fmax*(ct**2))/(m*np.pi**2)) + ((g*((ct**2))/8))
			vtc2 = ((v/3.6)*ct)/2
			deltaL = L - np.sqrt((L**2)-vtc2**2) + deltayc
			# deltay = ((Fmax*ct**2)/(m*np.pi**2))+g*((ct**2)/8)
			kleg = Fmax/deltaL
			kvert = Fmax/deltayc
			stiffness.append(kleg)
			stiffnessCM.append(kvert)
			F.append(-Fmax)
			DL.append(-deltaL)
			DY.append(-deltayc)
		K = np.mean(stiffness)
		Kstd = np.std(stiffness)
		df = pd.DataFrame()
		df['subject'] = [os.path.basename(file)[:2]]
		df['vel'] = [v]
		df['gait'] = gait.upper()
		df['leg'] = leg
		df['kleg'] = K
		df['CVkleg'] = Kstd/K
		df['kvert'] = np.mean(stiffnessCM)
		df['Fmax'] = [np.mean(F)]
		df['deltaL'] = [np.mean(DL)]
		df['deltaY'] = [np.mean(DY)]
		dfs.append(df)
stiff = pd.concat(dfs,ignore_index=True)
stiff.to_pickle('./stiffness.pickle')