import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.linear_model import LinearRegression


''' Es importante correr primero el script SkippingAssymetries.py porque es el que define el subset de datos con el que nos vamos a quedar'''

cot = pd.read_pickle('./CoT.pickle')
cot['velmsec'] = np.round(cot.vel/3.6,2)
data = cot[~cot.gait.isin(['W'])]
outlayers = ['FB','RI','AA']
data = data[~(data.subject.isin(outlayers))]# | ~(data.gait.isin(['S'])))]
assymetries = pd.read_pickle('./assymetries.pickle')
# assymetries['velmsec'] = np.round(data.vel/3.6,2)
s1 = pd.merge(assymetries,data, how='inner', on=['subject','gait','vel'])[['subject','gait','vel','COT','pot']]
data = pd.concat([data[data.gait=='R'],s1],ignore_index=True)
data['velmsec'] = np.round(data.vel/3.6,2)
data.to_pickle('./CoT_subset.pickle')



subjects = list(data.subject.unique())
gaits = list(data.gait.unique())
fitlist = []

for i,variable in enumerate(['pot','COT']):
	for gait in gaits:
		subset = data[data.gait==gait]
		for subject in subjects:
			tofit = subset[subset.subject==subject].sort_values(by='vel')
			if not tofit.empty:
				x = tofit['velmsec'].values.reshape(-1,1)
				y = tofit[variable].values
				# ypot = tofit['pot'].values
				# modelcot = LinearRegression().fit(x, ycot)
				model = LinearRegression().fit(x, y)
				df = pd.DataFrame()
				df['subject'] = [subject]
				df['gait'] = [gait]
				df['variable'] = [variable]
				df['intercept'] = [model.intercept_]
				df['slope'] = model.coef_
				df['r_2'] = [model.score(x, y)]
				fitlist.append(df)
fitcoeff = pd.concat(fitlist,ignore_index=True)
# fitcoeff.to_pickle('./fitcoeff.pickle')

datameans = data.groupby(['gait','vel']).mean()
fitlistmean = []

for variable in ['pot','COT']:
	for gait in gaits:
		xmean = datameans.loc[gait].sort_values(by='velmsec')['velmsec'].values.reshape(-1,1)
		ymean = datameans.loc[gait].sort_values(by='velmsec')[variable].values
		modelmean = LinearRegression().fit(xmean, ymean)
		df = pd.DataFrame()
		df['gait'] = [gait]
		df['variable'] = [variable]
		df['intercept'] = [modelmean.intercept_]
		df['slope'] = modelmean.coef_
		df['r_2'] = [modelmean.score(xmean, ymean)]
		fitlistmean.append(df)
fitcoeffmean = pd.concat(fitlistmean,ignore_index=True)


fitlist = []
for variable in ['pot', 'COT']:
	for gait in gaits:
		subset = data[data.gait==gait].sort_values(by='velmsec')
		x = subset['velmsec'].values.reshape(-1,1)
		y = subset[variable].values
		model = LinearRegression().fit(x, y)
		df = pd.DataFrame()
		df['gait'] = [gait]
		df['variable'] = [variable]
		df['intercept'] = [model.intercept_]
		df['slope'] = model.coef_
		df['r_2'] = [model.score(x, y)]
		fitlist.append(df)
fitcoeffALL = pd.concat(fitlist,ignore_index=True)
# fitcoeffALL.to_pickle('./fitcoeffALL.pickle')


regressions = {'fitcoeff':fitcoeff, 'fitcoeffmean':fitcoeffmean, 'fitcoeffALL':fitcoeffALL}
np.save('./regressionsMet.npy',regressions)