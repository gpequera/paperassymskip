#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import seaborn as sns
import numpy  as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

markers = pd.read_pickle('./foot_markers1.pickle').reset_index()
# markersR = pd.read_pickle('./foot_markers_R.pickle').reset_index()
# markers = [markersS, markersR]
fig, ax = plt.subplots(1,1,figsize=(6.5,6))
# fig = plt.figure(figsize=(6, 6))
# gs = fig.add_gridspec(6, 1,hspace=0.05)
# fig.subplots_adjust(hspace=0.05)
ax2=ax.twinx()
# ax.get_shared_y_axes().join(ax, ax2)
# ax = fig.add_subplot(gs[3:, 0])
# ax2 = fig.add_subplot(gs[:3, 0])
naranja_claro = sns.color_palette("Oranges_r")[-3]
naranja_oscuro = sns.color_palette("Oranges_r")[0]
verde = sns.color_palette('Set2')[0]
verde_oscuro = sns.color_palette("BuGn_r")[2]
verde_claro = sns.color_palette("BuGn_r")[-3]
t = np.arange(0,(markers.shape[0]-1)*(1/100),1/100)
ax.plot(t, markers.trANy, label='Trailing ', color=naranja_oscuro,linewidth=4)
ax.plot(t, markers.ldANy, label='Leading', color=naranja_claro,linewidth=4)
ax2.plot(t, markers.trHEEz, label='Trailing', color=naranja_oscuro, linewidth=1.5, linestyle='dotted', alpha=.3)
ax2.plot(t, markers.ldHEEz, label='Leading', color=naranja_claro, linewidth=1.5, linestyle='dotted', alpha=.3)
ax2.plot([t[0],t[0]], [50, 300],':', color = [.5,.5,.5,.5])
ax2.plot([t[markers.ldHEEz.idxmin()], t[markers.ldHEEz.idxmin()]], [50, 300],':', color = [.5,.5,.5,.5])
# ax.plot([t[0],t[0]], [0, 1200],':', color = [.5,.5,.5,.5])
# ax.plot([t[markers.ldHEEz.idxmin()], t[markers.ldHEEz.idxmin()]], [0, 1200],':', color = [.5,.5,.5,.5])
ax.legend(frameon=False)
x_tail = 0
y_tail = 0
x_head = t[markers.ldHEEz.idxmin()]
y_head = 0
dx = x_head - x_tail
dy = y_head - y_tail
Tpendulararrow = mpatches.FancyArrowPatch((x_tail, y_tail), (x_head, y_head), color=naranja_oscuro, mutation_scale=15)
ax.add_patch(Tpendulararrow)
ax.text(0.08,50,r'$t_{tr \rightarrow ld}$', ha="center", va="center", fontsize=12)

x_tail = t[markers.ldHEEz.idxmin()]
y_tail = 0
x_head = t[-1]
y_head = 0
dx = x_head - x_tail
dy = y_head - y_tail
Tbouncingarrow = mpatches.FancyArrowPatch((x_tail, y_tail), (x_head, y_head), color=naranja_claro, mutation_scale=15)
ax.add_patch(Tbouncingarrow)
ax.text(0.45,50,r'$t_{ld \rightarrow tr}$', ha="center", va="center", fontsize=12)

x_tail = t[markers.ldHEEz.idxmin()]
y_tail = markers.trANy[markers.ldHEEz.idxmin()]
x_head = t[markers.ldHEEz.idxmin()]
y_head = markers.ldANy[markers.ldHEEz.idxmin()]
dx = x_head - x_tail
dy = y_head - y_tail
Lpendulararrow = mpatches.FancyArrowPatch((x_tail, y_tail), (x_head, y_head), color=naranja_oscuro, mutation_scale=15)
ax.add_patch(Lpendulararrow)
ax.text(0.17,500,r'$l_{tr \rightarrow ld}$', ha="center", va="center", fontsize=12)

x_tail = 0
y_tail = -100
x_head = t[-1]
y_head = -100
dx = x_head - x_tail
dy = y_head - y_tail
Tstridearrow = mpatches.FancyArrowPatch((x_tail, y_tail), (x_head, y_head), color='k', mutation_scale=15)
ax.add_patch(Tstridearrow)
ax.text(0.3,-150,r'$t_{stride}$', ha="center", va="center", fontsize=12)

# ax.text(0.17,200,r'$l_{tr \rightarrow ld}$', ha="center", va="center", fontsize=12))

ax2.text(0.01,350,'Heel \n strike $tr$', ha="center", va="center")
ax2.text(t[markers.ldHEEz.idxmin()],350,'Heel \n strike $ld$', ha="center", va="center")

ax.spines.right.set_visible(False)
ax.spines.top.set_visible(False)
ax2.spines.left.set_visible(False)
ax2.spines.bottom.set_visible(False)
ax2.spines.top.set_visible(False)
ax2.set_xticklabels([])
ax2.set_xticks([])
ax.set_xlabel('Time (s)')
ax.set_ylabel('Ankle marker position (mm) \n (antero-posterior direction)')
ax2.set_ylabel('Heel marker position (mm) \n (vertical direction)')
# ax.set_title(subtitle[i])
ax.set_ylim([-200,1200])
plt.tight_layout()
plt.savefig('./figures/markers.pdf',bbox_inches='tight')
plt.savefig('./figures/markers.png',bbox_inches='tight')
plt.savefig('./figures/Fig1.pdf',bbox_inches='tight')
plt.show()



