import pandas as pd
import numpy as np
import pingouin as pg
from statsmodels.formula.api import ols
# import matplotlib.pyplot as plt 
# import seaborn as sns
# import scikit_posthocs as sp
# from itertools import combinations


data = pd.read_pickle('./assymetries.pickle')
data['velmsec'] = np.round(data.vel/3.6,2)
data = data[data.gait=='S']
normal_list = []
levene_list = []

lista_anovas = []
lista_kruskals = []
lista_welch = []
# listapairwises = []

# gaits = data.gait.unique()


##Normallity test

for variable in ['assymt', 'assyml', 'Lsteptr', 'Lstepld', 'Tsteptr', 'Tstepld']:
	# for gait in gaits:
	# dataG = data[data.gait==gait]
	formula = variable + ' ~ C(vel)'
	model = ols(formula, data=data).fit()
	norm = pg.normality(model.resid)
	# norm['gait'] = [gait]
	norm['variable'] = [variable]
	normal_list.append(norm)
normals = pd.concat(normal_list, ignore_index=True)


# ANOVA or Kruskal-Wallis test
## compare means (or medians in case of non-normality)

for index, row in normals.iterrows():
# 	gait = row['gait']
# 	dataG = data[data.gait==gait]
	if row['normal']:
		# variable =		
		equal_var = pg.homoscedasticity(data,dv=row['variable'], group='velmsec')
		if equal_var['equal_var'].iloc[0]:
			anova = pg.anova(dv=row['variable'], between='velmsec', data=data)
# 			anova['gait'] = [gait]
			anova['variable'] = [row['variable']]
			lista_anovas.append(anova)
		else:
			welch = pg.welch_anova(dv=row['variable'], between='velmsec', data=data)
# 			welch['gait'] = [gait]
			welch['variable'] = [row['variable']]
			lista_welch.append(welch)
	else:
		kruskal = pg.kruskal(dv=row['variable'], between='velmsec', data=data)
# 		kruskal['gait'] = [gait]
		kruskal['variable'] = [row['variable']]
		lista_kruskals.append(kruskal)

if lista_anovas:
	anovas = pd.concat(lista_anovas,ignore_index=True)
if lista_welch:
	welches = pd.concat(lista_welch,ignore_index=True)
if lista_kruskals:
	kruskals = pd.concat(lista_kruskals,ignore_index=True)