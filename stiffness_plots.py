import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pingouin as pg


k = pd.read_pickle('./stiffness.pickle')
k['gaitLeg'] = k['gait']+k['leg']
data = k[k.gaitLeg.isin(['Str','Sld','Rld' ])]
data['velmsec'] = np.round(data.vel/3.6,2)

dicc = np.load('./regressionsStiff.npy',allow_pickle=True).item()
fitcoeff = dicc['fitcoeffmean']

skipping = data[data.gait=='S']
trailing = skipping[skipping.leg=='tr'].sort_values(by=['subject','vel'])
leading = skipping[skipping.leg=='ld'].sort_values(by=['subject','vel'])
df = leading[['subject','vel','gait']]
df['asymmkleg'] = (trailing.kleg.values-leading.kleg.values)/leading.kleg.values
df['deltakleg'] = (trailing.kleg.values-leading.kleg.values)
# df['CVasymmkleg'] = (trailing.kleg.values-leading.kleg.values)/(leading.kleg.values)
df['asymmFmax'] = (trailing.Fmax.values-leading.Fmax.values)
df['asymml'] = (trailing.deltaL.values-leading.deltaL.values)
df['velmsec'] = np.round(df.vel/3.6,2)

# df = df[~df.subject.isin(outlayers)]

err_kws = {'elinewidth':.5,'capsize':2, 'ecolor':(.5,.5,.5)}

naranja = sns.color_palette("Oranges_r")[1]
verde = sns.color_palette('Set2')[0]
naranja_claro = sns.color_palette("Oranges_r")[-3]
naranja_oscuro = sns.color_palette("Oranges_r")[0]
paleta = [verde,naranja_claro, naranja_oscuro]

fig,ax = plt.subplots(1,2,figsize=(7.5,3),sharey=False)

ax = ax.ravel()
hue_order = ['Rld','Sld', 'Str']
sns.lineplot(x="velmsec", y='kleg',  data=data,hue="gaitLeg", hue_order=hue_order, palette= paleta,ax=ax[0],estimator=None,
	units='subject', linestyle='', marker='o', markersize=4, alpha=0.5 )
sns.lineplot(x="velmsec", y='kleg',  data=data,hue="gaitLeg", hue_order=hue_order, palette= paleta,ax=ax[0],ci='sd',
	err_style='bars', err_kws =err_kws, linestyle='', marker='o',markeredgecolor='k' )
textcoordY = [55000,50000,45000]
for i, gaitLeg in enumerate(hue_order):
	subset = data[data.gaitLeg==gaitLeg]
	x = subset.velmsec.unique()
	fitted = fitcoeff[fitcoeff.gaitLeg.isin([gaitLeg])]
	y = fitted['slope'].values*(x)+fitted['intercept'].values
	ax[0].plot(x,y,color=paleta[i])
	fittedeq = '$y = $'+str(fitted['slope'].values[0].round(2))+'$x$ + ' + str(fitted['intercept'].values[0].round(2))
	ax[0].text(1.39,textcoordY[i],fittedeq, fontsize=8, color=paleta[i])


ax[0].legend().remove()
handles, labels = ax[0].get_legend_handles_labels()
for h in handles: 
    h.set_marker('o')
    h.set_linestyle('')
    h.set_markeredgecolor('k')
labels = ['Running','Skipping (leading)', 'Skipping (trailing)',]
ax[0].legend(handles[0:3], labels,bbox_to_anchor=(.8, .7), frameon=False, fontsize=9) 
ax[0].set_xlabel('Speed (m.sec$^{-1}$)')
ax[0].set_ylabel('Stiffness (N/m)')
ax[0].set_xticks(data.velmsec.unique())
ax[0].set_xticklabels(data.velmsec.unique())
ax[0].text(1.39,29000,'#')
ax[0].text(1.81,29000,'#, †')
ax[0].text(2.4,29000,'#, †')
ax[0].text(1.39, 60000, 'A', fontsize=12)
ax[1].text(1.39, 4, 'B', fontsize=12)

sns.lineplot(x="velmsec", y='asymmkleg',  data=df, color=naranja,ax=ax[1],estimator=None, units= 'subject', linewidth=.5, linestyle='', marker='o',markersize=4,alpha=.5 )
sns.lineplot(x="velmsec", y='asymmkleg',  data=df, color=naranja,ax=ax[1],ci='sd',err_style='bars', err_kws =err_kws, linestyle='--', marker='o',markeredgecolor='k' )

ax[1].set_xlabel('Speed (m.sec$^{-1}$)')
ax[1].set_ylabel('Skipping leg \n stiffness asymmetry')
ax[1].set_xticks(df.velmsec.unique())
ax[1].set_xticklabels(df.velmsec.unique())



# for ii,variable in enumerate(['asymmkleg','deltakleg']):
# 	aov = pg.anova(dv=variable, between='vel', data=df)
# 	print('=======================================')
# 	print(variable)
# 	print(aov.round(3))
# sk = subset[subset.gait=='S']
# sk.anova(dv='kleg', between=['leg','vel']).round(3)
# pg.pairwise_ttests(dv='kleg', between=['leg', 'vel'], data=sk).round(3)

sns.set(style="white")
sns.set_context("paper", font_scale=1.3)
sns.despine()
sns.set_style("ticks")
plt.tight_layout()
plt.savefig('./figures/stiffness.pdf',bbox_inches='tight')
plt.savefig('./figures/stiffness.png',bbox_inches='tight')
plt.savefig('./figures/Fig5.pdf',bbox_inches='tight')
plt.show()
