#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import seaborn as sns
import numpy  as np
import matplotlib.pyplot as plt
import glob
import os

folder = './data/'
DFS = []
listademalos = []
malos = pd.DataFrame()
Lflight = []
DeltaLength = []
skippings = glob.glob(folder+'*_S_*.npy')
runnings = glob.glob(folder+'*_R_*.npy')
walkings = glob.glob(folder+'*_W_*.npy')
files = runnings+skippings
subset = [s for s in files if 'SS_S_' in s]
for file in skippings:
    file_id = os.path.basename(file)[:-4]
    data = np.load(file,encoding='latin1',allow_pickle=True).item()
    info = data.get('info')
    m = info['mass (kg)'].values.item()
    freq = [s for s in list(info.columns) if 'fs_kin' in s]
    fs = info[freq].values.item()
    # fs = info['fs_kin'].values.item()
    trailing = info['trailing_leg'].values.item()
    subject = file_id[:2]
    vel = info['vel (km/h)'].values.item()
    gait = info['gait'].values.item().upper()
    dt = 1/fs
    if trailing == 'l':
        side = 'kinematic_l'
        col = ['LANy','RANy','LHEEz','RHEEz']
    else:
        side = 'kinematic_r'
        col = ['RANy','LANy','RHEEz','LHEEz']
    DeltaStep = []
    Tstep1 = []
    Tstep2 = []
    Lstep = []
    Lflight =[]
    DeltaLength =[]
    if len(data[side])>20:
        middlestride = int(np.round(len(data[side])/2))
        pasos = data[side][middlestride-10:middlestride+10]
    else:
        pasos = data[side][1:-1]

    for i,stride in enumerate(pasos):
        markers = stride[col].reset_index(drop=True)
        markers.columns = ['trANy', 'ldANy', 'trHEEz', 'ldHEEz']
        # number = markers.trANy.argmax()
        # markers = pd.concat([markers.iloc[number:,:],markers.iloc[:number,:]],ignore_index=True)
        stridetime = markers.shape[0]*dt
        stridelength = stridetime*vel/(3.6)
        HeelStrike_ld = markers['ldHEEz'].argmin()
        t_tr2ld = HeelStrike_ld*dt
        t_ld2tr = (markers.index[-1]-HeelStrike_ld)*dt
        Tstep1.append(t_tr2ld)
        Tstep2.append(t_ld2tr)
        deltat = (t_ld2tr-t_tr2ld)/(t_tr2ld+t_ld2tr)
        # deltat = (t2-t1)
        DeltaStep.append(deltat)
        l1 = (markers['ldANy'].iloc[HeelStrike_ld]-markers['trANy'].iloc[HeelStrike_ld])/1000
        isbad = markers['trANy'].iloc[0]-markers['ldANy'].iloc[0]
        if gait == 'R':
            l2 = markers['trANy'].iloc[-1]-markers['ldANy'].iloc[-1]
        else:
            # l2 = markers['ldANy'].iloc[HeelStrike_ld]-markers['trANy'].iloc[-1]
            l2 = stridelength-l1
            malos = pd.DataFrame()
            if isbad>0:
                malos['subject'] = [subject]
                malos['gait'] = [gait]
                malos['vel'] = [vel]
                malos['paso'] = [i+1]
                listademalos.append(malos)
        if isbad<0:
            deltal = (l1-l2)/(l1+l2)
            # deltal = (l1-l2)
            Lstep.append(l1)
            Lflight.append(l2)
            DeltaLength.append(deltal)
    if DeltaLength:
        Tsteptr = np.mean(Tstep1)
        Tstepld = np.mean(Tstep2)
        assymt = np.mean(DeltaStep)
        Ltr = np.mean(Lstep)
        Lld = np.mean(Lflight)
        assyml = np.mean(DeltaLength)
        # if assyml<.60:
        #     print(info['file_name'])
        #     print(trailing)
        df = pd.DataFrame()
        df['subject'] = [subject]
        df['gait'] = [gait]
        df['vel'] = [vel]
        df['Tsteptr'] = [Tsteptr/stridetime]
        df['Tstepld'] = [Tstepld/stridetime]
        df['ST'] = [stridetime]
        df['assymt'] = [assymt]
        df['Lsteptr'] = [Ltr/stridelength]
        df['Lstepld'] = [Lld/stridelength]
        df['SL'] = [stridelength]
        # df['SLrel'] = [stridelength/stridelength] 
        df['assyml'] = [assyml]
        df['n_pasos'] = [len(DeltaLength)]
        df['file_id'] = [file_id]
        DFS.append(df)
if listademalos:
    badskip = pd.concat(listademalos,ignore_index=True)
    badskip.to_pickle('./badskip.pickle')



assymetries = pd.concat(DFS,ignore_index=True).sort_values(by=['subject','vel']).reset_index(drop=True)
# assymetries = assymetries.dropna()
assymetries = assymetries[assymetries['n_pasos']>10]
assymetries.to_pickle('./assymetries.pickle')


