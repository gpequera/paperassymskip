import pandas as pd
import numpy as np
import pingouin as pg
from statsmodels.formula.api import ols
# import matplotlib.pyplot as plt 
# import seaborn as sns
import scikit_posthocs as sp
from itertools import combinations


data =pd.read_pickle('./CoT_subset.pickle')

normal_list = []
levene_list = []

lista_anovas = []
lista_kruskals = []
lista_welch = []
# listapairwises = []

gaits = data.gait.unique()


##Normallity test

for variable in ['pot', 'COT']:
	for gait in gaits:
		dataG = data[data.gait==gait]
			# vels = dataG.velmsec.unique()
			# for v in vels:
		# stats = dataG[dataG.gait.isin([v])]
		formula = variable + ' ~ C(vel)'
		model = ols(formula, data=dataG).fit()
		norm = pg.normality(model.resid)
		norm['gait'] = [gait]
		norm['variable'] = [variable]
		normal_list.append(norm)
normals = pd.concat(normal_list, ignore_index=True)


# ANOVA or Kruskal-Wallis test
## compare means (or medians in case of non-normality)

for index, row in normals.iterrows():
	gait = row['gait']
	dataG = data[data.gait==gait]
	if row['normal']:		
		equal_var = pg.homoscedasticity(dataG,dv=row['variable'], group='velmsec')
		if equal_var['equal_var'].iloc[0]:
			anova = pg.anova(dv=row['variable'], between='velmsec', data=dataG)
			anova['gait'] = [gait]
			anova['variable'] = [row['variable']]
			lista_anovas.append(anova)
		else:
			welch = pg.welch_anova(dv=row['variable'], between='velmsec', data=dataG)
			welch['gait'] = [gait]
			welch['variable'] = [row['variable']]
			lista_welch.append(welch)
	else:
		kruskal = pg.kruskal(dv=row['variable'], between='velmsec', data=dataG)
		kruskal['gait'] = [gait]
		kruskal['variable'] = [row['variable']]
		lista_kruskals.append(kruskal)

if lista_anovas:
	anovas = pd.concat(lista_anovas,ignore_index=True)
if lista_welch:
	welches = pd.concat(lista_welch,ignore_index=True)
if lista_kruskals:
	kruskals = pd.concat(lista_kruskals,ignore_index=True)