import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.linear_model import LinearRegression


''' Es importante correr primero el script SkippingAssymetries.py porque es el que define el subset de datos con el que nos vamos a quedar'''

stiffness = pd.read_pickle('./stiffness.pickle')
stiffness['gaitLeg'] = stiffness['gait']+stiffness['leg']
data = stiffness[stiffness.gaitLeg.isin(['Str','Sld','Rld' ])]
mps = data.vel.values
data['velmsec'] = np.round(mps/3.6,2)


subjects = list(data.subject.unique())
gaitLegs = list(data.gaitLeg.unique())
fitlist = []

for gaitLeg in gaitLegs:
	subset = data[data.gaitLeg==gaitLeg]
	for subject in subjects:
		tofit = subset[subset.subject==subject].sort_values(by='vel')
		if not tofit.empty:
			x = tofit['velmsec'].values.reshape(-1,1)
			y = tofit['kleg'].values
			model = LinearRegression().fit(x, y)
			df = pd.DataFrame()
			df['subject'] = [subject]
			df['gaitLeg'] = [gaitLeg]
# 			df['variable'] = [variable]
			df['intercept'] = [model.intercept_]
			df['slope'] = model.coef_
			df['r_2'] = [model.score(x, y)]
			fitlist.append(df)
fitcoeff = pd.concat(fitlist,ignore_index=True)
# fitcoeff.to_pickle('./fitcoeff.pickle')

datameans = data.groupby(['gaitLeg','vel']).mean()
fitlistmean = []

for gaitLeg in gaitLegs:
	xmean = datameans.loc[gaitLeg].sort_values(by='velmsec')['velmsec'].values.reshape(-1,1)
	ymean = datameans.loc[gaitLeg].sort_values(by='velmsec')['kleg'].values
	modelmean = LinearRegression().fit(xmean, ymean)
	df = pd.DataFrame()
	df['gaitLeg'] = [gaitLeg]
# 	df['variable'] = [variable]
	df['intercept'] = [modelmean.intercept_]
	df['slope'] = modelmean.coef_
	df['r_2'] = [modelmean.score(xmean, ymean)]
	fitlistmean.append(df)
fitcoeffmean = pd.concat(fitlistmean,ignore_index=True)


fitlist = []
for gaitLeg in gaitLegs:
	subset = data[data.gaitLeg==gaitLeg].sort_values(by='velmsec')
	x = subset['velmsec'].values.reshape(-1,1)
	y = subset['kleg'].values
	model = LinearRegression().fit(x, y)
	df = pd.DataFrame()
	# df['gaitLeg'] = [gaitLeg]
	# df['variable'] = [variable]
	df['intercept'] = [model.intercept_]
	df['slope'] = model.coef_
	df['r_2'] = [model.score(x, y)]
	fitlist.append(df)
fitcoeffALL = pd.concat(fitlist,ignore_index=True)
# # fitcoeffALL.to_pickle('./fitcoeffALL.pickle')


regressions = {'fitcoeff':fitcoeff, 'fitcoeffmean':fitcoeffmean, 'fitcoeffALL':fitcoeffALL}
np.save('./regressionsStiff.npy',regressions)