#Stiffness stats
import pandas as pd
import numpy as np
import pingouin as pg
from statsmodels.formula.api import ols
import matplotlib.pyplot as plt 
import seaborn as sns
import scikit_posthocs as sp
from itertools import combinations

stiff = pd.read_pickle('./stiffness.pickle')
stiff = stiff[~stiff.gait.isin(['W'])]  
stiff['gaitLeg'] = stiff.gait + stiff.leg
stiff = stiff[~stiff.gaitLeg.isin(['Rtr'])]
stiff['velmsec'] = np.round(stiff.vel.values/3.6,2)

normal_list = []
levene_list = []

listaanovas = []
listakrukals = []
listapairwises = []

for v in [1.39,  1.81, 2.5]:
	stats = stiff[stiff.velmsec.isin([v])]
	formula = 'kleg ~ C(gaitLeg)'
	model = ols(formula, data=stats).fit()
	norm = pg.normality(model.resid)
	norm['vel'] = [v]
	levene = pg.homoscedasticity(stats, dv='kleg', group='gaitLeg')
	levene['vel'] = [v]
	norm['levene'] = levene['equal_var'].item()
	normal_list.append(norm)
	levene_list.append(levene)
	if len(stats.gaitLeg.unique())> 2:
		if norm.normal[0]:
			if ~norm['levene'][0]:
				anovaGait = pg.welch_anova(data=stats, dv='kleg', between='gaitLeg').round(3)
				# anovaGait['vel'] = [v]
			else:
				anovaGait = pg.anova(data=stats, dv='kleg', between='gaitLeg', detailed=True).round(3).drop([1])
			anovaGait['vel'] = [v]
			listaanovas.append(anovaGait)
			if anovaGait['p-unc'][0]<0.05:
				pairwiseGait = pg.pairwise_ttests(dv='kleg', between='gaitLeg', data=stats, padjust='bonf',parametric=True).round(3)
				pairwiseGait['vel'] = [v] * pairwiseGait.shape[0]
				listapairwises.append(pairwiseGait)
		else:
			kruskalGait = pg.kruskal(data=stats, dv='kleg', between='gaitLeg', detailed=True).round(3)
			kruskalGait['vel'] = [v]
			listakrukals.append(kruskalGait)
			if kruskalGait['p-unc'][0]<0.05:
				pairwiseGait = pg.pairwise_ttests(dv='kleg', between='gaitLeg', data=stats, padjust='bonf',parametric=False).round(3)
				pairwiseGait['vel'] = [v] * pairwiseGait.shape[0]
				listapairwises.append(pairwiseGait)
	else:
		pairwiseGait = pg.pairwise_ttests(dv='kleg', between='gaitLeg', data=stats, padjust='bonf',parametric=norm.normal[0]).round(3)
		pairwiseGait['vel'] = [v] * pairwiseGait.shape[0]
		listapairwises.append(pairwiseGait)

		 

kruskals = pd.concat(listakrukals,ignore_index=True)
pairwises = pd.concat(listapairwises,ignore_index=True)
normal = pd.concat(normal_list,ignore_index=True)
levene_test = pd.concat(levene_list,ignore_index=True)


#Stats by vel

normal_list = []
# levene_list = []

lista_anovas = []
lista_kruskals = []
lista_welch = []
# listapairwises = []

gaitLegs = stiff.gaitLeg.unique()


##Normallity test

# for variable in ['pot', 'COT']:
for gaitLeg in gaitLegs:
	dataG = stiff[stiff.gaitLeg==gaitLeg]
		# vels = dataG.velmsec.unique()
		# for v in vels:
	# stats = dataG[dataG.gait.isin([v])]
	formula = 'kleg ~ C(vel)'
	model = ols(formula, data=dataG).fit()
	norm = pg.normality(model.resid)
	norm['gaitLeg'] = [gaitLeg]
	# norm['variable'] = [variable]
	normal_list.append(norm)
normals = pd.concat(normal_list, ignore_index=True)

#ANOVA or Kruskal-Wallis test
# compare means (or medians in case of non-normality)

for index, row in normals.iterrows():
	gaitLeg = row['gaitLeg']
	dataG = stiff[stiff.gaitLeg==gaitLeg]
	if row['normal']:		
		equal_var = pg.homoscedasticity(dataG,dv='kleg', group='velmsec')
		if equal_var['equal_var'].iloc[0]:
			anova = pg.anova(dv='kleg', between='velmsec', data=dataG)
			anova['gaitLeg'] = [gaitLeg]
			# anova['variable'] = [row['variable']]
			lista_anovas.append(anova)
		else:
			welch = pg.welch_anova(dv='kleg', between='velmsec', data=dataG)
			welch['gaitLeg'] = [gaitLeg]
			# welch['variable'] = [row['variable']]
			lista_welch.append(welch)
	else:
		kruskal = pg.kruskal(dv='kleg', between='velmsec', data=dataG)
		kruskal['gaitLeg'] = [gaitLeg]
		# kruskal['variable'] = [row['variable']]
		lista_kruskals.append(kruskal)

if lista_anovas:
	anovasByvel = pd.concat(lista_anovas,ignore_index=True)
if lista_welch:
	welchesByVel = pd.concat(lista_welch,ignore_index=True)
if lista_kruskals:
	kruskalsByVel = pd.concat(lista_kruskals,ignore_index=True)