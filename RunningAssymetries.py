#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import seaborn as sns
import numpy  as np
import matplotlib.pyplot as plt
import glob
import os

folder = './data/'
DFS = []
listademalos = []
malos = pd.DataFrame()
Lflight = []
DeltaLength = []
skippings = glob.glob(folder+'*_S_*.npy')
runnings = glob.glob(folder+'*_R_*.npy')
walkings = glob.glob(folder+'*_W_*.npy')
files = runnings+skippings
subset = [s for s in files if 'SS_S_' in s]
for file in runnings:
    file_id = os.path.basename(file)[:-4]
    data = np.load(file,encoding='latin1',allow_pickle=True).item()
    info = data.get('info')
    m = info['mass (kg)'].values.item()
    freq = [s for s in list(info.columns) if 'fs_kin' in s]
    fs = info[freq].values.item()
    # fs = info['fs_kin'].values.item()
    trailing = info['trailing_leg'].values.item()
    subject = file_id[:2]
    vel = info['vel (km/h)'].values.item()
    gait = info['gait'].values.item().upper()
    dt = 1/fs
    if trailing == 'l':
        side = 'kinematic_l'
        col = ['LANy','RANy','LHEEz','RHEEz']
    else:
        side = 'kinematic_r'
        col = ['RANy','LANy','RHEEz','LHEEz']
    DeltaStep = []
    Tstep1 = []
    Tstep2 = []
    Lstep = []
    Lflight =[]
    DeltaLength =[]
    if len(data[side])>20:
        middlestride = int(np.round(len(data[side])/2))
        pasos = data[side][middlestride-10:middlestride+10]
    else:
        pasos = data[side][1:-1]

    for i,stride in enumerate(pasos):
        markers = stride[col].reset_index(drop=True)
        markers.columns = ['trANy', 'ldANy', 'trHEEz', 'ldHEEz']
        stridetime = markers.shape[0]*dt
        stridelength = stridetime*vel/(3.6)
        HeelStrike_ld = markers['ldHEEz'].argmin()
        t_tr2ld = HeelStrike_ld*dt
        t_ld2tr = (markers.index[-1]-HeelStrike_ld)*dt
        Tstep1.append(t_tr2ld)
        Tstep2.append(t_ld2tr)
        deltat = (t_ld2tr-t_tr2ld)/(t_tr2ld+t_ld2tr)
        DeltaStep.append(deltat)
        l1 = t_tr2ld*(vel/3.6)
        l2 = t_ld2tr*(vel/3.6)
        deltal = (l1-l2)/(l1+l2)
        Lstep.append(l1)
        Lflight.append(l2)
        DeltaLength.append(deltal)
    Tsteptr = np.mean(Tstep1)
    Tstepld = np.mean(Tstep2)
    assymt = np.mean(DeltaStep)
    Ltr = np.mean(Lstep)
    Lld = np.mean(Lflight)
    assyml = np.mean(DeltaLength)   
    df = pd.DataFrame()
    df['subject'] = [subject]
    df['gait'] = [gait]
    df['vel'] = [vel]
    df['Tsteptr'] = [Tsteptr/stridetime]
    df['Tstepld'] = [Tstepld/stridetime]
    df['ST'] = [stridetime]
    df['assymt'] = [assymt]
    df['Lsteptr'] = [Ltr/stridelength]
    df['Lstepld'] = [Lld/stridelength]
    df['SL'] = [stridelength]
    df['assyml'] = [assyml]
    df['n_pasos'] = [len(DeltaLength)]
    df['file_id'] = [file_id]
    DFS.append(df)




assymetries = pd.concat(DFS,ignore_index=True).sort_values(by=['subject','vel']).reset_index(drop=True)
assymetries = assymetries.dropna()
assymetries = assymetries[assymetries['n_pasos']>10]
assymetries.to_pickle('./assymetriesRunning.pickle')


